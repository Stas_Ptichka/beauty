﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(beautyPro.Startup))]
namespace beautyPro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //hello
        }
    }
}
